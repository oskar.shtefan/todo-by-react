import React from'react'

import TodoListItem from "../todo-list-item";

const  TodoList = () => {
    return (
        <ul>
            <li>
                <TodoListItem prop={'drink cola'} />{/*into the prop u can put anything*/}
            </li>
            <li>
                <TodoListItem prop={'read book'}/>{/*into the prop u can put anything*/}
            </li>
            <li>
                <TodoListItem prop= {'drive car'} />{/*into the prop u can put anything*/}
            </li>
        </ul>
    )
}
export default TodoList