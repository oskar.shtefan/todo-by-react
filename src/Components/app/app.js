import React from'react'
import TodoTittle from "../todo-tittle/todo-tittle";
import TodoList from "../todo-list/todo-list";
import TodoListItem from "../todo-list-item/todo-list-item";
import SearchPanel from "../search-pannel";
import './app.css'

const App = () => {


    return (
        <div className='todo-app'>
            <TodoTittle/>
            <TodoList/>
            <TodoListItem/>
            <SearchPanel/>
        </div>
    )
}
export default App